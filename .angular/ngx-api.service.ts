import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from "@angular/common/http";
import { catchError, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class NgxApiService {

  constructor(private http: HttpClient) { }

  public get<T>(apiUrl: string): Observable<T> {
    return this.http
      .get<T>(apiUrl, { responseType: 'json' })
      .pipe(catchError((error: any) => {
        throw new Error(error.message)
      }))
  }

  public post<T>(apiUrl: string, body: any): Observable<T> {
    return this.http
      .post<T>(apiUrl, body, {headers:{skip:"true"}})
      .pipe(catchError((error: any) => {
        throw new Error(error.message)
      }))
  }
}
