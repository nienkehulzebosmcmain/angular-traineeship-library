import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { Observable, tap } from "rxjs";
import { IndexDbService } from './indexdb.service';

@Injectable({
  providedIn: 'root'
})
export class NgxApiService {
  public networkStatusEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private http: HttpClient, private indexDbService: IndexDbService) {
    this.indexDbService.open('pwa', 1, [{ name: 'requests', keypath: 'body' }])
  }

  public get<T>(apiUrl: string): Observable<T> {
    return this.http
      .get<T>(apiUrl, { responseType: 'json' })
      .pipe(
        tap({
          error: (err: HttpErrorResponse) => {
            this.handleError(err);
          },
          complete: () => {
            this.handleComplete();
          }
        })
      )
  }

  public post<T>(apiUrl: string, body: object): Observable<T> {
    return this.http
      .post<T>(apiUrl, body, { headers: { skip: "true" } })
      .pipe(
        tap({
          error: (err: HttpErrorResponse) => {
            this.indexDbService.add('requests', { url: apiUrl, body: JSON.stringify(body) })
            this.handleError(err);
          },
          complete: () => {
            this.handleComplete();
          }
        })
      );
  }

  private sendQueuedRequests() {
    const getAllrequest = this.indexDbService.getAll('requests');

    if (getAllrequest == undefined) {
      return;
    }

    getAllrequest.onsuccess = () => {
      if (getAllrequest.result != undefined) {
        getAllrequest.result.forEach((request: { url: string, body: string; }) => {
          this.http.post(request.url, JSON.parse(request.body), { headers: { skip: "true" } }).subscribe({
            complete: () => {
              this.indexDbService.delete('requests', request.body);
            }
          })
        })
      };
    }
  }

  private handleError(err: HttpErrorResponse) {
    if (err.status == 504) {
      this.networkStatusEmitter.emit(false);
    }
    else {
      throw new Error(err.message);
    }
  }

  private handleComplete() {
    this.sendQueuedRequests();
    this.networkStatusEmitter.emit(true);
  }
}
