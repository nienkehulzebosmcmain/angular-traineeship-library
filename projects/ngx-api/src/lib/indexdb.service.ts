
// This source has been heavily used for making this service: https://javascript.info/indexeddb

import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class IndexDbService {
    private database!: IDBDatabase;

    constructor() { }

    public open(name: string, version: number, objectstores?: [{ name: string, keypath: string }]) {
        let request = indexedDB.open(name, version);

        // Object stores can only be created/removed/modified in the onupgradeneeded handler
        request.onupgradeneeded = () => {
            let db = request.result;

            if (objectstores != undefined) {
                objectstores.forEach(store => {
                    db.createObjectStore(store.name, { keyPath: store.keypath });
                });
            }
        };

        request.onerror = () => {
            throw new Error(request.error?.message);
        };

        request.onsuccess = () => {
            let db = request.result;

            this.database = db;

            // When the current databse version is outdated
            db.onversionchange = () => {
                db.close();
                window.location.reload();
            };
        };

        request.onblocked = () => {
            // this event shouldn't trigger if we handle onversionchange correctly

            // it means that there's another open connection to the same database
            // and it wasn't closed after db.onversionchange triggered for it
            throw new Error('A newer version of the database needs to be loaded. Please close all other tabs and reload the page.')
        };
    }

    public add(objectStoreName: string, value: object) {
        let transaction = this.database.transaction(objectStoreName, 'readwrite');
        let store = transaction.objectStore(objectStoreName);

        let request = store.add(value);

        request.onerror = () => {
            throw new Error(request.error?.message);
        };
    }

    public getAll(objectStoreName: string): IDBRequest | undefined {
        if (this.database == undefined) {
            return undefined;
        }

        let transaction = this.database.transaction(objectStoreName); // readonly
        let store = transaction.objectStore(objectStoreName);

        let request = store.getAll();

        request.onerror = () => {
            throw new Error(request.error?.message);
        };

        return request;
    }

    // Delete an entry from the objectstore. 
    public delete(objectStoreName: string, key: string) {
        let transaction = this.database.transaction(objectStoreName, 'readwrite');
        let store = transaction.objectStore(objectStoreName);

        let request = store.delete(key);

        request.onerror = () => {
            throw new Error(request.error?.message);
        };
    }
}