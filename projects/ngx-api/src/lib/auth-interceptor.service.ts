import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { NgxAuthenticationService } from "ngx-authentication";

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

    constructor(private _ngxAuthenticationService: NgxAuthenticationService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.headers.get('skip')) {
            if (this._ngxAuthenticationService.canActivate()) {
                return next.handle(req.clone({ setHeaders: { "Authorization": "Bearer " + this._ngxAuthenticationService.getAuthenticationCookie() } }));
            }
        }
        return next.handle(req);
    }
}