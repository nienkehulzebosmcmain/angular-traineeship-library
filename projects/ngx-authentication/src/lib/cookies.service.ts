import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CookiesService {

  constructor() { }

  public getCookie(key: string): string {
    const cookie: string | undefined = document.cookie
    .split("; ")
    .find((row) => row.startsWith(`${key}=`))
    ?.split("=")[1];

    return cookie == undefined ? '': cookie;
  }

  public setCookie(key: string, value: string) {
    if (!this.isCookieSet(key)) {
      document.cookie = `${key}=${value}; max-age=60*30; secure`; // Max-age has precedence when both max-age and expiration date are set. 
    }
  }

  public deleteCookie(key: string) {
    if (this.isCookieSet(key)) {
      document.cookie = `${key}=; max-age=0; path=/; domain=${location.hostname}`
    }
  }

  public isCookieSet(key: string): boolean {
    if (this.getCookie(key) != '') {
      return true;
    }
    return false;
  }
}
