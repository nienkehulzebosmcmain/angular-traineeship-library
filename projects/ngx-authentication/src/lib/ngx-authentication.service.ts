import { Injectable } from "@angular/core";
import { Router, UrlTree } from "@angular/router";
import { CookiesService } from "./cookies.service";

@Injectable({
    providedIn: 'root'
})
export class NgxAuthenticationService {
    private readonly _authorisationKey = 'authorisation';

    constructor(private _cookieService: CookiesService, private _router: Router) { }

    public setAuthenticationCookie(authToken: string): void {
        this._cookieService.setCookie(this._authorisationKey, authToken);
    }

    public getAuthenticationCookie(): string {
        return this._cookieService.getCookie(this._authorisationKey);
    }

    public deleteAuthenticationCookie() {
        this._cookieService.deleteCookie(this._authorisationKey);
    }

    public canActivate(): boolean | UrlTree {
        if (this._cookieService.isCookieSet(this._authorisationKey)) {
            return true;
        }
        return this._router.createUrlTree(['login'])
    }
}